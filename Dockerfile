FROM centos:centos7.6.1810

RUN yum -y install epel-release
# make sure to install the correct release here, otherwise you run into GPG key errors
RUN yum -y install http://repo.okay.com.mx/centos/7/x86_64/release/okay-release-1-5.el7.noarch.rpm
RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*

RUN yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*

RUN yum -y install https://harbottle.gitlab.io/epypel/7/x86_64/epypel-release.rpm
RUN rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY*

RUN yum -y update ; yum clean all

RUN \
  yum -y install curl gcc wim python3 python3-pip python-is-python3 python3-devel net-tools && \
  yum -y remove git git-*
RUN yum -y install \
https://repo.ius.io/ius-release-el7.rpm \
https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
  yum -y install git224

RUN \
  yum -y install wget make cmake3 gcc-c++ && \
  yum clean all
RUN curl -sL https://rpm.nodesource.com/setup_10.x | bash - && \
  yum -y install nodejs && \
  npm install --global yarn
RUN yum makecache && \
  yum -y install ninja-build

# install devtoolset-8
RUN yum -y install centos-release-scl
RUN yum -y install devtoolset-8
RUN /usr/bin/scl enable devtoolset-8 bash

# add devtoolset to PATH and LD_LIBRARY_PATH
ENV PATH=/opt/rh/devtoolset-8/root/usr/bin${PATH:+:${PATH}}
ENV LD_LIBRARY_PATH /opt/rh/devtoolset-8/root/usr/lib64


# Install Intel OneAPI 
RUN \
cd /tmp && \
wget  https://registrationcenter-download.intel.com/akdlm/irc_nas/17769/l_BaseKit_p_2021.2.0.2883.sh && \
wget  https://registrationcenter-download.intel.com/akdlm/irc_nas/17764/l_HPCKit_p_2021.2.0.2997.sh
# wget https://registrationcenter-download.intel.com/akdlm/irc_nas/17766/l_AIKit_p_2021.2.0.1101.sh
RUN cd /tmp && sh l_BaseKit_p_2021.2.0.2883.sh -s -a -s --eula accept \
--action install --components \ 
intel.oneapi.lin.dpcpp-cpp-compiler:\
intel.oneapi.lin.mkl.devel

RUN cd /tmp && sh l_HPCKit_p_2021.2.0.2997.sh -s -a -s --eula accept && \
rm -rf /tmp/*

RUN yum -y install openssl-devel libffi-devel bzip2-devel zlib-devel 
RUN yum -y install sqlite-devel
RUN wget https://www.python.org/ftp/python/3.9.7/Python-3.9.7.tgz && \
    tar -xzf Python-3.9.7.tgz && \
    cd Python-3.9.7 && \
    ./configure --enable-optimizations && \
    make altinstall
RUN yum -y install makedepf90 openssl-static


# # Add gitlab-runner user
RUN \
  adduser gitlab-runner && \
  passwd -d gitlab-runner && \
  chown -R gitlab-runner:gitlab-runner /home/gitlab-runner

# Run script to set up shell environment for intel compilers on entry
COPY entrypoint.sh /root/entrypoint.sh
#RUN chown gitlab-runner:gitlab-runner /home/gitlab-runner/entrypoint.sh

# set ulimit -s
RUN ulimit -s unlimited
RUN ln -s /usr/bin/cmake3 /usr/local/cmake
ENV INTEL_MKL_PATH /opt/intel/oneapi/mkl/2021.2.0/lib/intel64/
ENV LD_PRELOAD $INTEL_MKL_PATH/libmkl_core.so:$INTEL_MKL_PATH/libmkl_intel_lp64.so:$INTEL_MKL_PATH/libmkl_sequential.so
#USER gitlab-runner
#WORKDIR /home/gitlab-runner

# install Rust for gitlab-runner user
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

ENTRYPOINT ["/root/entrypoint.sh"]
CMD ["/bin/bash"]

